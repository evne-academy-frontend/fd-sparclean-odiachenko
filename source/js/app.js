

console.log("Hello world!");

function showAlert() {
    alert("Button clicked!");
}


const price_recurring = 33; //price
const price_one_off = 40;
const price_end = 40;

const time_bedroom_recuring = 20; //time
const time_bedroom_one_off = 45;
const time_bedroom_end = 70;

const time_bathroom_recuring = 40; //time
const time_bathroom_one_off = 85;
const time_bathroom_end = 130;

const slight_markings_or_dirt = 0.75;
const moderate_making_or_dirt = 1;
const heavy_markings_or_dirt = 1.55;

var time_bedrooms, time_bathrooms, price, condition;

function calcTime (count_bed, count_bath, condition, clean_type ) {
 switch (clean_type) {
  case "recurring":
    time_bedrooms = time_bedroom_recuring;
    time_bathrooms = time_bathroom_recuring;
    price = price_recurring;
     break;
  case "one_off":
    time_bedrooms = time_bedroom_one_off;
    time_bathrooms = time_bathroom_one_off;
    price = price_one_off;
     break;
  case "end":
    time_bedrooms = time_bedroom_end;
    time_bathrooms = time_bathroom_end;
    price = price_end;
       break;
}
 var calcTime = (time_bedrooms*count_bed + time_bathrooms*count_bath)/60;
 var time = calcTime*parseFloat(condition);
 calcPrice(time,price);
 return time;

}

function calcPrice(time, price) {
  console.log(time*price);
  return  time*price;

}

var result = calcTime(3,1,0.75,"end");
var res = calcPrice(2, 33);

console.log(result, res);

// var map;
//   function initMap() {
//   var map = new google.maps.Map(document.getElementById('map'), {
//       center: {lat: -34.397, lng: 150.644},
//       zoom: 8,
//     })
//     var locations = [
//     {lat: -31.563910, lng: 147.154312},
//     {lat: -33.718234, lng: 150.363181},
//     {lat: -33.727111, lng: 150.371124},
//     {lat: -33.848588, lng: 151.209834},
//     {lat: -33.851702, lng: 151.216968},
//     {lat: -34.671264, lng: 150.863657},
//     {lat: -35.304724, lng: 148.662905},
//     {lat: -36.817685, lng: 175.699196},
//     {lat: -36.828611, lng: 175.790222},
//     {lat: -37.750000, lng: 145.116667},
//     {lat: -37.759859, lng: 145.128708},
//     {lat: -37.765015, lng: 145.133858},
//     {lat: -37.770104, lng: 145.143299},
//     {lat: -37.773700, lng: 145.145187},
//     {lat: -37.774785, lng: 145.137978},
//     {lat: -37.819616, lng: 144.968119},
//     {lat: -38.330766, lng: 144.695692},
//     {lat: -39.927193, lng: 175.053218},
//     {lat: -41.330162, lng: 174.865694},
//     {lat: -42.734358, lng: 147.439506},
//     {lat: -42.734358, lng: 147.501315},
//     {lat: -42.735258, lng: 147.438000},
//     {lat: -43.999792, lng: 170.463352}
//   ]
//
//      var labels = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
//      var markers = locations.map(function(location, i) {
//           return new google.maps.Marker({
//             position: location,
//             label: labels[i % labels.length]
//           });
//         });
//
//         var markerCluster = new MarkerClusterer(map, markers,
//             {imagePath: 'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m'});
// }


var map;
  function initMap() {
    var center = new google.maps.LatLng(-34.546019, 150.857020);
    var map = new google.maps.Map(document.getElementById('map'), {
      center: center,
      zoom: 8,
    });

    var image = {
      url: './source/img/map.svg',
      size: new google.maps.Size(30,40),
      scaledSize: new google.maps.Size(30,40),
    }

    var marker = new google.maps.Marker({
      position: center,
      map: map,
      icon: image,
    });
    var contentString = '<div id="content">' +
    '<h3>Sparclean Services</h3>' +
    '<div id="bodyContent">' +
    '<p><strong>Address:</strong> 1 Short St, Lake Illawarra 2528, Australia</p>' +
    '<p><strong>Phone:</strong> 0467 323 979</p>' +
    '<p><strong>E-mail:</strong> hello@sparclean.com.au</p>' +
    '</div>' +
    '</div>' ;

    var infowindow = new google.maps.InfoWindow({
      content: contentString
    });

    marker.addListener('click', function (){
      infowindow.open(map, marker);
    });

  }


  $(document).ready(function(){
  $('.users-slider').slick({
    slidesToShow: 2,
    dots: true,

  });
});
